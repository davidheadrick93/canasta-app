package db

import "gitlab.com/davidheadrick93/canasta-app/graph/model"

type DB struct {
	TeamScores map[string]*model.Score
}

func NewDB() *DB {
	return &DB{
		TeamScores: make(map[string]*model.Score, 4),
	}
}

func (d *DB) GetScore(id string) *model.Score {
	return d.TeamScores[id]
}

// TODO: validate score inputs
// TODO: write code comments for funcs/types

func (d *DB) AddScore(input model.ScoreInput) *model.Score {
	if _, found := d.TeamScores[input.Team.ID]; !found {
		score := &model.Score{
			Total: 0,
			Team: &model.Team{
				ID:   input.Team.ID,
				Name: input.Team.Name,
			},
		}
		d.TeamScores[input.Team.ID] = score
	}

	d.TeamScores[input.Team.ID].Total += input.Count + input.Base

	return d.TeamScores[input.Team.ID]
}
